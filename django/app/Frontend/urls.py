from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from . import views

urlpatterns = [
    path('', views.landpage),
    path('distpred/', include("DistPred.urls")),
    path('admin/', admin.site.urls),
]
