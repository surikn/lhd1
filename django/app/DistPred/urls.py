from django.urls import path
from . import views

urlpatterns = [
    path("", views.landpage),
    path("custom", views.custom_predict_page),
    path("update_world_data", views.update_world_data),
    path("get_world_data", views.get_world_data),
    path("get_total_data", views.get_total_data),
    path("get_country_data", views.get_country_data),
    path("predict_custom", views.predict_custom)
]

