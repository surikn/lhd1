import pandas as pd
import numpy as np
import seaborn as sns
import plotly.express as px
import plotly.graph_objects as go
from sklearn.preprocessing import StandardScaler, LabelEncoder, OneHotEncoder
from sklearn.linear_model import Ridge
from scipy.optimize import curve_fit
import json
import lightgbm as lgb
from datetime import datetime
import os
# used to not to recalculate all date statistics when predicting recursively
max_shift = 8
max_shift_delta = pd.Timedelta(days=max_shift)

num_features = ['recovered_lag_2',
 'dead_lag_4',
 'lag3_minus_max',
 'recovered_poly_pred',
 'rolling_confirmed_min',
 'testpop',
 'recovered_poly_1',
 'Death rate, crude (per 1,000 people)',
 'dead_lag_1',
 'rolling_recovered_mean',
 'hospibed',
 'lag1_minus_max',
 'Population, total',
 'GDP ($ per capita)',
 'Cause of death, by communicable diseases and maternal, prenatal and nutrition conditions (% of total)',
 'Mortality rate attributed to household and ambient air pollution, age-standardized (per 100,000 population)',
 'rolling_confirmed_std',
 'tests',
 'Mortality from CVD, cancer, diabetes or CRD between exact ages 30 and 70 (%)',
 'rolling_recovered_min',
 'Poverty headcount ratio at $3.20 a day (2011 PPP) (% of population)',
 'Mortality rate, adult, female (per 1,000 female adults)',
 'Tuberculosis treatment success rate (% of new cases)',
 'confirmed_ridge_bias',
 'recovered_poly_0',
 'recovered_lag_7',
 'International migrant stock, total',
 'recovered_lag_1',
 'recovered_poly_3',
 'confirmed_poly_3',
 'recovered_ridge_coef',
 'rolling_dead_min',
 'Infant mortality (per 1000 births)',
 'confirmed_poly_2',
 'recovered_ridge_pred',
 'rolling_dead_mean',
 'Number of people spending more than 25% of household consumption or income on out-of-pocket health care expenditure',
 'Population in urban agglomerations of more than 1 million (% of total population)',
 'dead_lag_7',
 'Survival to age 65, male (% of cohort)',
 'dead_ridge_pred',
 'Number of people spending more than 10% of household consumption or income on out-of-pocket health care expenditure',
 'Labor force participation rate, total (% of total population ages 15+) (modeled ILO estimate)',
 'recovered_expanding_max',
 'dead_lag_2',
 'recovered_lag_4',
 'dead_poly_0',
 'Smoking prevalence, females (% of adults)',
 'Population ages 15-64 (% of total)',
 'Survival to age 65, female (% of cohort)',
 'Cause of death, by non-communicable diseases (% of total)',
 'schools',
 'confirmed_lag_2',
 'GDP per capita, PPP (current international $)',
 'confirmed_poly_0',
 'recovered_poly_2',
 'Life expectancy at birth, total (years)',
 'Hospital beds (per 1,000 people)',
 'Mortality rate, adult, male (per 1,000 male adults)',
 'temperature',
 'rolling_recovered_std',
 'recovered_lag_5',
 'rolling_recovered_max',
 'People using at least basic sanitation services (% of population)',
 'dead_lag_5',
 'Mortality rate attributed to unsafe water, unsafe sanitation and lack of hygiene (per 100,000 population)',
 'Trade (% of GDP)',
 'Diabetes prevalence (% of population ages 20 to 79)',
 'Population density (people per sq. km of land area)',
 'People using safely managed sanitation services (% of population)',
 'Tuberculosis case detection rate (%, all forms)',
 'confirmed_lag_6',
 'dead_expanding_max',
 'rolling_dead_std',
 'PM2.5 air pollution, population exposed to levels exceeding WHO guideline value (% of total)',
 'Incidence of tuberculosis (per 100,000 people)',
 'People with basic handwashing facilities including soap and water (% of population)',
 'rolling_dead_max',
 'Net migration',
 'medianage',
 'recovered_lag_6',
 'confirmed_expanding_max',
 'dead_ridge_coef',
 'Population in the largest city (% of urban population)',
 'lag2_minus_max',
 'Out-of-pocket expenditure (% of current health expenditure)',
 'dead_ridge_bias',
 'confirmed_lag_5',
 'confirmed_lag_1',
 'confirmed_lag_3',
 'dead_poly_2',
 'quarantine',
 'dead_lag_3',
 'confirmed_lag_4',
 'rolling_confirmed_max',
 'restrictions',
 'Current health expenditure per capita, PPP (current international $)',
 'Urban population (% of total)',
 'confirmed_ridge_pred',
 'confirmed_poly_pred',
 'days_since_quar_start',
 'recovered_ridge_bias',
 'confirmed_ridge_coef',
 'dead_lag_6',
 'Air transport, passengers carried',
 'confirmed_lag_7',
 'Population ages 65 and above (% of total)',
 'dead_poly_pred',
 'International tourism, number of departures',
 'Prevalence of HIV, total (% of population ages 15-49)',
 'dead_poly_1',
 'recovered_lag_3',
 'confirmed_poly_1',
 'Smoking prevalence, males (% of adults)',
 'International tourism, number of arrivals',
 'rolling_confirmed_mean',
 'dead_poly_3']

cat_features = ['quar_type', 'Region']

features  = num_features + cat_features


data = pd.read_pickle("/app/data_part2.pkl")


last_train_day = pd.to_datetime('2020/04/17')
last_test_day = pd.to_datetime('2020/12/31')
targets = ['confirmed', 'recovered', 'dead']


max_shift = 8
max_shift_delta = pd.Timedelta(days=max_shift)

def ridge_features(y):
    # returns Ridge params and predictions a week ahead
    pred_day = 8

    if (y.isna().any()) or (y == 0).all():
        return pd.Series(np.zeros((3,)), index=['ridge_bias', 'ridge_coef', 'ridge_pred'])        
    x = np.arange(1, len(y) + 1).reshape(len(y), -1)
    y = y[::-1]
    r = Ridge()
    r.fit(x, y)
    pred = r.predict([[pred_day]])[0]
    return pd.Series([r.coef_[0], r.intercept_, pred], index=['ridge_bias', 'ridge_coef', 'ridge_pred'])

def poly_features(y):
    # fit polynomial to data, to better see growth
    # returns poly coeffs and pred
    deg = 3  # polynomial up to deg order
    pred_day = 8 # day we make predictions for
    cnames = [f'poly_{i}' for i in range(deg + 1)] + ['poly_pred']
    if (y.isna().any()) or (y == 0).all():
        return pd.Series(np.zeros((len(cnames),)), index=cnames)
    x = np.arange(1, len(y) + 1)
    y = y[::-1]
    params = np.polyfit(x, y, deg)[::-1]
    pred = np.polyval(params[::-1], [pred_day])
    return pd.Series(np.append(params, pred), index=cnames)

def add_time_features(data):
    num_features = []
    cat_features = []
    
    # add lag features
    lags = [1, 2, 3, 4, 5, 6, 7]
    for lag in lags:
        lag_features = data.groupby('Country/Region')[['confirmed', 'dead', 'recovered']].shift(lag)
        lag_features.columns = [f'{col}_lag_{lag}' for col in lag_features.columns]
        num_features += lag_features.columns.to_list()
        data.drop(lag_features.columns, axis=1, inplace=True, errors='ignore')
        data = pd.concat([data, lag_features], axis=1)

    # rolling statistics
    window = 7
    rollings = data.groupby('Country/Region')[['confirmed', 'dead', 'recovered']].shift(1).rolling(window).agg(['mean', 'std', 'max', 'min'])
    rollings.columns = ['rolling_' + '_'.join(i) for i in rollings.columns]
    num_features += rollings.columns.to_list()
    data.drop(rollings.columns, axis=1, inplace=True, errors='ignore')
    data = pd.concat([data, rollings], axis=1)

    # max target delta expanding
    for t in targets:
        data[t + '_expanding_max'] = data.groupby('Country/Region')[t].transform(lambda x: x.shift(1).expanding().max())
        num_features += [t + '_expanding_max']

    #  max_confirmed - lag1_confirmed, max_confirmed - lag2_confirmed

    lag_minus_max_features = ['lag1_minus_max', 'lag2_minus_max', 'lag3_minus_max']
    lag_minus_max = [1, 2, 3]
    for l, col_name in zip(lag_minus_max, lag_minus_max_features):
        data[col_name] = data['confirmed_expanding_max'] - data[f'confirmed_lag_{l}']
    num_features += lag_minus_max_features

    if True:
        # ridge regression and polynomial on lags for confirmed, recovered and dead
        for t in ['confirmed_', 'recovered_', 'dead_']:
            t_lags = data.columns[data.columns.str.contains(t + 'lag_')]
            rfs = data[t_lags].apply(ridge_features, axis=1, result_type='expand')
            rfs.columns = [t + i for i in rfs.columns]
            data.drop(rfs.columns, axis=1, inplace=True, errors='ignore')
            data = pd.concat([data, rfs], axis=1)
            num_features += rfs.columns.to_list()

            pfs = data[t_lags].apply(poly_features, axis=1, result_type='expand')
            pfs.columns = [t + i for i in pfs.columns]
            data.drop(pfs.columns, axis=1, inplace=True, errors='ignore')
            data = pd.concat([data, pfs], axis=1)
            num_features += pfs.columns.to_list()

    return data, num_features, cat_features


class CovidPredictor(object):

    def __init__(self, last_train_day, data=None):
        self.targets = targets
        self.models = {}
        for t in self.targets:
            self.models[t] = lgb.Booster(model_file=f'/app/DistPred/models/{t}.txt')
        self.last_train_day = last_train_day
        if data is None:
            self.data = pd.read_pickle('/app/data_part2.pkl')
        else:
            self.data = data
        self.simulation_data = self.data.copy()

    def set_quarantine(self, when, where):
        self.simulation_data.loc[(self.simulation_data['iso_alpha2'] == where) & (self.simulation_data['date'] >= when), 'quarantine'] = 1.0

    def close_departure(self, when, where):
        self.simulation_data.loc[(self.simulation_data['iso_alpha2'] == where) & (self.simulation_data['date'] >= when), 'International tourism, number of departures'] = 0.0

    def close_arrivals(self, when, where):
        self.simulation_data.loc[(self.simulation_data['iso_alpha2'] == where) & (self.simulation_data['date'] >= when), 'International tourism, number of arrivals'] = 0.0

    def dump_simulation(self):
        self.simulaton_data = self.data.copy()

    def _run_lgb(self, test):
        preds = pd.DataFrame()
        for key in self.models.keys():
            pred = np.maximum(0, self.models[key].predict(test[features]))
            pred = np.round(pred).astype('int')
            preds[key] = pred
        return preds


    def predict(self, h):
        # returns train_data + h predicted days
        # h - forecast horizont
        data = self.simulation_data.copy()

        for i in range(1, h + 1):
            pred_day = self.last_train_day + pd.Timedelta(days=i)
            # print(pred_day)

            # recalculate time_features for pred_day
            recalc_window = (data['date'] >= pred_day - max_shift_delta) & (data['date'] <= pred_day)
            recalced = add_time_features(data.loc[recalc_window])[0]
            data.loc[data['date'] == pred_day, :] = recalced.loc[recalced['date'] == pred_day, :]
            xt = data[data['date'] == pred_day]
            data.loc[data['date'] == pred_day, self.targets] = self._run_lgb(xt).values

        return data[data['date'] <= pred_day]


    def predict_single_country(self, h, country):
        # returns train_data + h predicted days for country
        # h - forecast horizont
        data = self.simulation_data.copy()
        data = data[data['iso_alpha2'] == country]

        for i in range(1, h + 1):
            pred_day = self.last_train_day + pd.Timedelta(days=i)
            # print(pred_day)

            # recalculate time_features for pred_day
            recalc_window = (data['date'] >= pred_day - max_shift_delta) & (data['date'] <= pred_day)
            recalced = add_time_features(data.loc[recalc_window])[0]
            data.loc[data['date'] == pred_day, :] = recalced.loc[recalced['date'] == pred_day, :]
            xt = data[data['date'] == pred_day]
            data.loc[data['date'] == pred_day, self.targets] = self._run_lgb(xt).values

        return data[data['date'] <= pred_day]


def jsonize_world_data(preds):
    covid_world_timeline = []
    preds = preds[preds["iso_alpha2"].isna() == False]
    for ind, date in enumerate(preds.date.unique()):
        date_obj = {}
        t = pd.to_datetime(str(date)) 
        timestring = t.strftime('%Y-%m-%d')
        date_obj["date"] = timestring

        date_list = []
        preds_date = preds[preds["date"] == date]
        for i in range(len(preds_date)):
            date_list.append({
                "confirmed": int(preds_date.iloc[i]["confirmed"]),
                "deaths": int(preds_date.iloc[i]["dead"]),
                "recovered": int(preds_date.iloc[i]["recovered"]),
                "id": preds_date.iloc[i]["iso_alpha2"], 
            })

        date_obj["list"] = date_list
        covid_world_timeline.append(date_obj)
    return covid_world_timeline


def jsonize_total_data(preds):
    covid_total_timeline = []
    for ind, date in enumerate(preds.date.unique()):
        t = pd.to_datetime(str(date)) 
        timestring = t.strftime('%Y-%m-%d')
        covid_total_timeline.append({
            "date": timestring,
            "confirmed": int(preds.loc[preds["date"] == date, "confirmed"].sum()),
            "deaths": int(preds.loc[preds["date"] == date, "dead"].sum()),
            "recovered": int(preds.loc[preds["date"] == date, "recovered"].sum()),
        })
    return covid_total_timeline
