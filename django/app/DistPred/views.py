from django.shortcuts import render
from django.http import HttpRequest, HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from .predictor import CovidPredictor, jsonize_total_data, jsonize_world_data
import pandas as pd
from django.views.decorators.csrf import csrf_exempt
from datetime import datetime
import json
# Create your views here.i

def landpage(request):
	return render(request, "DistPred/landpage.html", {
		"world_data": open("/app/jsonized_world_data.json").read(),
		"total_data": open("/app/jsonized_total_data.json").read(),
	})

@csrf_exempt
def get_world_data(request):
	return HttpResponse(open("/app/jsonized_world_data.json").read(), content_type="application/json")

@csrf_exempt
def get_total_data(request):
	return HttpResponse(open("/app/jsonized_total_data.json").read(), content_type="application/json")

@csrf_exempt
def update_world_data(request):
	last_train_day = pd.to_datetime('2020/04/17')
	predictor = CovidPredictor(last_train_day)
	predictions = predictor.predict(14)
	
	for code in predictions.code.unique():
		predictions.loc[predictions["code"] == code, "confirmed"] = predictions.loc[predictions["code"] == code, "confirmed"].cumsum()
		predictions.loc[predictions["code"] == code, "recovered"] = predictions.loc[predictions["code"] == code, "recovered"].cumsum()
		predictions.loc[predictions["code"] == code, "dead"] = predictions.loc[predictions["code"] == code, "dead"].cumsum()

	jsonized_world_data = jsonize_world_data(predictions)
	jsonized_total_data = jsonize_total_data(predictions)

	open("/app/jsonized_world_data.json", "w").write(json.dumps(jsonized_world_data))
	open("/app/jsonized_total_data.json", "w").write(json.dumps(jsonized_total_data))

	return HttpResponse(json.dumps({"ok": True}), content_type="application/json")

@csrf_exempt
def get_country_data(request):
	if "country" not in request.GET:
		return HttpResponse(json.dumps({"ok": False, "error": "Invalid request"}), content_type="application/json")
	setable_params = pd.read_csv("/app/setable_params.csv")
	return HttpResponse(json.dumps({
		"arrival": setable_params[setable_params["iso_alpha2"] == request.GET["country"]].values.flatten()[3],
		 "departure": setable_params[setable_params["iso_alpha2"] == request.GET["country"]].values.flatten()[2],
		 "hospital_beds": 123,
		 "quarantine": setable_params[setable_params["iso_alpha2"] == request.GET["country"]].values.flatten()[1],
		}), content_type="application/json")

def custom_predict_page(request):
	return render(request, "DistPred/cust.html")

def predict_custom(request):
	country = request.GET["country"]
	arrival = pd.to_datetime(request.GET["arrival"])
	quarantine = pd.to_datetime(request.GET["quarantine"])
	hospital_beds = request.GET["hospital_beds"]
	departure = pd.to_datetime(request.GET["departure"])
	
	last_train_day = pd.to_datetime('2020/04/17')
	predictor = CovidPredictor(last_train_day)

	predictor.set_quarantine(quarantine, country)
	predictor.close_departure(departure, country)
	predictor.close_arrivals(arrival, country)
	
	prediction = predictor.predict_single_country(7, country)
	prediction["confirmed"] = prediction["confirmed"].cumsum()
	prediction["recovered"] = prediction["recovered"].cumsum()
	prediction["dead"] = prediction["dead"].cumsum()

	response = {
		"dead": prediction["dead"].to_list(),
		"confirmed": prediction["confirmed"].to_list(),
		"recovered": prediction["recovered"].to_list(),
		"dates": list(map(lambda x: pd.to_datetime(str(x)).strftime('%Y-%m-%d'), prediction["date"]))
	}
	return HttpResponse(json.dumps(response), content_type="application/json") 
