from django.apps import AppConfig


class DistpredConfig(AppConfig):
    name = 'DistPred'
